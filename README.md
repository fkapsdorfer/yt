
## Commands
```
npm run build
electron dist/window.js
```

## References
- https://www.npmjs.com/package/class-transformer
- https://reactjs.org/docs/faq-structure.html
- https://www.typescriptlang.org/docs/handbook/compiler-options.html
- https://www.typescriptlang.org/docs/handbook/jsx.html
- https://www.typescriptlang.org/docs/handbook/react-&-webpack.html
- https://github.com/mui-org/material-ui
- https://github.com/mui-org/material-ui/tree/master/examples/create-react-app-with-typescript
- https://material.io/resources/icons/?style=baseline
- https://nodejs.org/dist/latest-v12.x/docs/api/fs.html#fs_fs_promises_api
- https://github.com/electron/electron-quick-start-typescript
- https://editorconfig.org/
- https://github.com/nestjs/nest/tree/master/sample
- https://www.npmjs.com/package/sqlite3
- https://github.com/typeorm/typeorm/tree/master/sample
- https://github.com/jplayer/react-jPlayer
- https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio
- https://github.com/ytdl-org/youtube-dl/blob/master/README.md#output-template
- https://github.com/ytdl-org/youtube-dl/blob/master/youtube_dl/extractor/youtube.py
