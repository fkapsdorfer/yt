import * as execa from 'execa';
import {plainToClass} from 'class-transformer';
import {inject, injectable, interfaces} from 'inversify';
import {TYPES} from '../types';
import {TrackAsDownloaded} from '../entity/as-downloaded';
import {ISettings} from './settings.d';
import {ITrackUrl, ITrackUrlCollection} from './track-url.d';
import {IYtService} from './yt-service.d';

@injectable()
export class YtService implements IYtService {
  @inject(TYPES.Settings) private _settings: ISettings;
  @inject(TYPES.TrackUrlCollection) private _trackUrls: ITrackUrlCollection;
  @inject(TYPES.TrackUrl) private _TrackUrl: interfaces.Newable<ITrackUrl>;

  async fetch(id: string): Promise<{trackObj: TrackAsDownloaded, url: string}> {
    const callResult = await execa(this._settings.youtubeDl, ['-j', `https://youtube.com/watch?v=${id}`]);
    const trackObj: TrackAsDownloaded = JSON.parse(callResult.all);

    const formats = trackObj.formats
      .filter((f => -1 < f.format.indexOf('audio only')))
      .sort((fa, fb) => fa.abr - fb.abr);
    const {url = null} = formats.find(f => f.abr === 128) || formats.pop() || {};

    delete trackObj.tags;

    const trackUrl = plainToClass(this._TrackUrl, {id, url});
    this._trackUrls.add(trackUrl);

    return {trackObj, url};
  }

  async getUrl(id: string): Promise<string> {
    return (this._trackUrls.get(id) || await this.fetch(id)).url;
  }
}
