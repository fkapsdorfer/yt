import {TrackAsDownloaded} from '../entity/as-downloaded';

export interface IYtService {
  fetch(id: string): Promise<{trackObj: TrackAsDownloaded, url: string}>;
  getUrl(id: string): Promise<string>;
}
