
export interface IPlaylist {
  id: string;
  maxLength: number;
  unflushed: number;
  direction: -1|1;

  name: string;
  filename: string;

  construct(o: {id: string, direction?: -1|1, maxLength?: number}): IPlaylist;
}

export interface IPlaylistCollection {
  collection: IPlaylist[];
  get(): IPlaylist[];
  get(id: string): IPlaylist;
  add(id: string): Promise<this>;
  remove(id: string): Promise<this>;
  load(): Promise<IPlaylist[]>;
}
