import * as assert from 'assert';
import {promises as fs} from 'fs';
import {classToPlain, Expose, plainToClassFromExist} from 'class-transformer';
import {injectable} from 'inversify';
import {ISettings} from './settings.d';

@injectable()
export class Settings implements ISettings {
  @Expose() youtubeDl: string = 'youtube-dl';
  @Expose() storageDir: string = '/run/media/filip/Data/Projects/docs/music';
  @Expose() historyMaxLength: number = 50;
  @Expose() addingDirection: 1|-1 = -1;
  @Expose() ttl: number = 6 * 60 * 60 * 1000;

  private _validate() {
    assert.ok(this.addingDirection === -1 || this.addingDirection === 1, '\'addingDirection\' has to be -1|1!');
    assert.ok(this.historyMaxLength > 0, '\'historyMaxLength\' must be a possitive integer!');
  }

  async load(): Promise<void> {
    const str = await fs.readFile(`${this.storageDir}/settings.json`, 'utf-8')
      .catch(err => err.code === 'ENOENT' ? null : Promise.reject(err));
    if (str) plainToClassFromExist(this, str);
    this._validate();
  }

  async save(): Promise<void> {
    this._validate();
    await fs.writeFile(`${this.storageDir}/settings.json`, JSON.stringify(classToPlain(this), undefined, 2));
  }
}
