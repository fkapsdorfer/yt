
export interface ITrackUrl {
  id: string;
  url: string;
  issueTime: Date;

  isValid(): boolean;
}

export interface ITrackUrlCollection {
  collection: ITrackUrl[];
  unflushed: number;
  filename: string;

  get(id: string): ITrackUrl;
  add(trackUrl: ITrackUrl): this;
  remove(id: string): this;
  load(): Promise<void>;
  save(): Promise<void>;
  saveSync(): this;
  canFlush(): boolean;
}
