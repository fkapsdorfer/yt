import {promises as fs} from 'fs';
import {inject, injectable, interfaces} from 'inversify';
import {iocc} from '../iocc';
import {TYPES} from '../types';
import {ISettings} from './settings.d';
import {IPlaylist, IPlaylistCollection} from './playlist.d';

@injectable()
export class Playlist implements IPlaylist {
  id = 'default';
  maxLength = null;
  unflushed = 0;
  direction: -1|1;

  construct(o: {id: string, direction?: -1|1, maxLength?: number}) {
    this.id = o.id;
    this.direction = o.direction || this._settings.addingDirection;
    this.maxLength = o.maxLength || Number.POSITIVE_INFINITY;
    return this;
  }

  get name() {return this.id}
  get filename() {return `${this._settings.storageDir}/${this.id}.json`}

  private get _settings(): ISettings {return iocc.get<ISettings>(TYPES.Settings)};
}

@injectable()
export class PlaylistCollection implements IPlaylistCollection {
  collection: IPlaylist[] = [];

  get(): IPlaylist[];
  get(id: string): IPlaylist;
  get(id?: string) {
    return id
      ? this.collection.find(p => p.id === id)
      : this.collection;
  }

  @inject(TYPES.Settings) private _settings: ISettings;
  @inject(TYPES.Playlist) private _Playlist: interfaces.Newable<IPlaylist>;

  async add(id: string): Promise<this> {
    const playlist = new this._Playlist().construct({id});
    const enoent = await fs.access(playlist.filename)
      .then(() => false, async e => e.code === 'ENOENT' || Promise.reject(e));
    if (!enoent)
      return this;
    await fs.writeFile(playlist.filename, '[]');
    await this.load();
    return this;
  }

  async remove(id: string): Promise<this> {
    const playlist = this.get(id);
    await fs.unlink(playlist.filename);
    this.collection = this.collection.filter(e => e !== playlist);
    return this;
  }

  async load(): Promise<IPlaylist[]> {
    const settings = this._settings;
    const re = /^(.*)\.json$/;
    return this.collection = (await fs.readdir(settings.storageDir))
      .map(e => e.match(re))
      .filter(e => e && e[1] !== 'track-urls' && e[1] !== 'settings')
      .map(([, id]) => new this._Playlist().construct({id,
          maxLength: id === 'history' ? settings.historyMaxLength : undefined,
      }));
  }
}
