import {promises as fs, writeFileSync} from 'fs';
import {deserializeArray, plainToClass, Transform} from 'class-transformer';
import {inject, injectable} from 'inversify';
import {iocc} from '../iocc';
import {TYPES} from '../types';
import {ITrackUrl, ITrackUrlCollection} from './track-url.d';
import {ISettings} from './settings.d';

@injectable()
export class TrackUrl implements ITrackUrl {
  id: string;
  url: string;
  @Transform((value?: string) => new Date(value || Date.now()), {toClassOnly: true})
  issueTime: Date = undefined; // undefined: class-transformer wouldn't call @transform

  private get _settings(): ISettings {return iocc.get<ISettings>(TYPES.Settings)};

  isValid() {
    return Date.now() < this.issueTime.getTime() + this._settings.ttl;
  }
}

@injectable()
export class TrackUrlCollection implements ITrackUrlCollection {
  collection: ITrackUrl[] = [];
  unflushed: number = 0;

  get filename() { return `${this._settings.storageDir}/track-urls.json` }

  @inject(TYPES.Settings) private _settings: ISettings;

  get(id: string): ITrackUrl {
    const trackUrl = this.collection.find(e => e.id === id);
    if (trackUrl && trackUrl.isValid())
      return trackUrl;
  }

  /** Exising entry will be overwritten. */
  add(trackUrl: ITrackUrl): this {
    trackUrl = plainToClass(TrackUrl, trackUrl);
    const existingIndex = this.collection.findIndex(e => e.id === trackUrl.id);
    const index = -1 < existingIndex ? existingIndex : this.collection.length;
    this.collection[index] = trackUrl;
    this.unflushed++;
    return this;
  }

  remove(id: string): this {
    this.collection = this.collection.filter(e => e.id !== id);
    return this;
  }
  
  async load(): Promise<void> {
    const str = await fs.readFile(this.filename, 'utf-8')
      .catch(e => e.code === 'ENOENT' ? Promise.resolve('[]') : Promise.reject(e));
    this.collection = deserializeArray(TrackUrl, str)
      .filter(e => e.isValid());
  }
  
  async save(): Promise<void> {
    if (!this.unflushed)
      return;
    this.collection = this.collection.filter(e => e.isValid());
    await fs.writeFile(this.filename, JSON.stringify(this.collection));
  }
  
  saveSync(): this {
    if (!this.unflushed)
      return;
    this.collection = this.collection.filter(e => e.isValid());
    writeFileSync(this.filename, JSON.stringify(this.collection));
    return this;
  }

  canFlush(): boolean {
    return !!this.unflushed;
  }
}
