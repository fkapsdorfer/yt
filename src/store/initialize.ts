import {ISettings} from './settings.d';
import {ITrackUrlCollection} from './track-url.d';
import {ITrackCollection} from './track.d';
import {IPlaylistCollection} from './playlist.d';
import {beforeExit} from '../hooks';
import {TYPES} from '../types';
import {iocc} from '../iocc';

export async function initPersistence(): Promise<void> {
  const settings = iocc.get<ISettings>(TYPES.Settings);
  const playlists = iocc.get<IPlaylistCollection>(TYPES.PlaylistCollection);
  const trackUrls = iocc.get<ITrackCollection>(TYPES.TrackCollection);
  const tracks = iocc.get<ITrackUrlCollection>(TYPES.TrackUrlCollection);

  await settings.load();
  await playlists.load();
  await trackUrls.load();
  await tracks.load();
  await playlists.add('history');

  beforeExit(() => {
    tracks.saveSync();
    trackUrls.saveSync();
  });
}
