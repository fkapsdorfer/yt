
export interface ISettings {
  youtubeDl: string;
  storageDir: string;
  historyMaxLength: number;
  addingDirection: -1|1;
  ttl: number;

  load(): Promise<void>;
  save(): Promise<void>;
}
