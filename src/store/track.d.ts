import {IPlaylist} from './playlist.d';

export interface ITrack {// PK: playlist, id
  id: string;
  playlist: string;
  title: string;
  uploader: string;
  duration: number;
  tags?: string[];

  getUrl(): Promise<string>;
  clone(): ITrack;
}

export interface ITrackCollection {
  collection: ITrack[];

  get(playlist?: string): ITrack[];
  get(playlist: string, id: string): ITrack;
  add(track: ITrack): this;
  remove(track: ITrack): this;
  remove(track: string, playlist: string): this;
  getJsonStr(playlist: string): string;
  load(playlist?: IPlaylist|string): Promise<void>;
  save(playlist?: IPlaylist|string): Promise<void>;
  saveSync(playlist?: IPlaylist|string): void;
  canFlush(playlist?: IPlaylist|string): boolean;

  construct(playlistId: string, id: string): Promise<this>;
}
