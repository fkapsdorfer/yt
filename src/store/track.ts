import {promises as fs, writeFileSync} from 'fs';
import {deserializeArray, plainToClass, classToPlain, Expose, Exclude} from 'class-transformer';
import {inject, injectable, interfaces} from 'inversify';
import {iocc} from '../iocc';
import {TYPES} from '../types';
import {IPlaylist, IPlaylistCollection} from './playlist.d';
import {ITrack, ITrackCollection} from './track.d';
import {IYtService} from './yt-service.d';

@injectable()
export class Track implements ITrack {
  @Expose() id: string;
  @Exclude() playlist: string = 'default';
  @Expose() title: string;
  @Expose() uploader: string;
  @Expose() duration: number;
  @Expose() tags?: string[];

  private get _ytService(): IYtService {return iocc.get<IYtService>(TYPES.YtService)};
  private get _Track(): interfaces.Newable<ITrack> {return iocc.get<interfaces.Newable<ITrack>>(TYPES.Track)};

  getUrl(): Promise<string> {
    return this._ytService.getUrl(this.id);
  }

  clone(): ITrack {
    return plainToClass(this._Track, this);
  }
}

@injectable()
export class TrackCollection implements ITrackCollection {
  collection: ITrack[] = [];

  @inject(TYPES.PlaylistCollection) private _playlists: IPlaylistCollection;
  @inject(TYPES.YtService) private _ytService: IYtService;
  @inject(TYPES.Track) private _Track: interfaces.Newable<ITrack>;

  get(playlist?: string): ITrack[];
  get(playlist: string, id: string): ITrack;
  get(playlist?: string, id?: string) {
    return playlist && id ? this.collection.find(t => t.playlist === playlist && t.id === id)
      : playlist ? this.collection.filter(t => t.playlist === playlist)
      : this.collection;
  }
  
  add(track: ITrack): this {
    const playlist = this._playlists.get(track.playlist);
    const duplicateIndex = this.collection.findIndex(t => t.playlist === track.playlist && t.id === track.id);
    if (-1 < duplicateIndex)
      this.collection.splice(duplicateIndex, 1);
    if (playlist.direction < 0) {
      if (playlist.maxLength <= this.collection.length)
        this.collection.pop();
      this.collection.unshift(track);
    } else {
      if (playlist.maxLength <= this.collection.length)
        this.collection.shift();
      this.collection.push(track);
    }
    playlist.unflushed++;
    return this;
  }

  remove(track: ITrack|string, playlist?: string): this {
    playlist = playlist || (track as ITrack).playlist;
    track = typeof track === 'string'
      ? track
      : track.id;
    this.collection = this.collection.filter(t => t.playlist !== playlist || t.id !== track);
    this._playlists.get(playlist).unflushed++;
    return this;
  }

  getJsonStr(playlist: string): string {
    const tracks = classToPlain(this.get(playlist));
    return JSON.stringify(tracks, null, 2);
  }

  async load(playlist?: IPlaylist|string): Promise<void> {
    if (!playlist)
      return void await Promise.all(this._playlists.collection.map(p => this.load(p)));
    if (typeof playlist === 'string')
      playlist = this._playlists.get(playlist);
    const str = await fs.readFile(playlist.filename, 'utf-8');
    const tracks = deserializeArray(Track, str);
    tracks.forEach(t => t.playlist = (playlist as IPlaylist).id);
    this.collection = this.collection.filter(t => t.playlist !== (playlist as IPlaylist).id);
    this.collection.push(...tracks);
  }

  async save(playlist?: IPlaylist|string): Promise<void> {
    if (!playlist)
      return void await Promise.all(this._playlists.collection.map(p => this.save(p)));
    if (typeof playlist === 'string')
      playlist = this._playlists.get(playlist);
    if (!playlist.unflushed)
      return;
    await fs.writeFile(playlist.filename, this.getJsonStr(playlist.id));
    playlist.unflushed = 0;
  }

  saveSync(playlist?: IPlaylist|string): void {
    if (!playlist)
      return void this._playlists.collection.forEach(p => this.saveSync(p));
    if (typeof playlist === 'string')
      playlist = this._playlists.get(playlist);
    if (!playlist.unflushed)
      return;
    writeFileSync(playlist.filename, this.getJsonStr(playlist.id));
    playlist.unflushed = 0;
  }

  canFlush(playlist?: IPlaylist|string): boolean {
    const playlists = typeof playlist === 'string' ? [this._playlists.get(playlist)]
      : playlist ? [playlist]
      : this._playlists.get();
    return playlists.some(p => p.unflushed);
  }

  async construct(playlistId: string, id: string): Promise<this> {
    const plain = (await this._ytService.fetch(id)).trackObj;
    const track = plainToClass(this._Track, plain, {excludeExtraneousValues: true});
    track.tags = [];
    track.playlist = playlistId;
    return this.add(track);
  }
}
