const registered: (() => void)[] = [];

export function beforeExit(fn: () => void) {
  registered.push(fn);
}

export function emitExit() {
  registered.forEach(fn => fn());
}
