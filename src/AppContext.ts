//import {createContext, useState} from 'react';
import {ITrack} from './store/track.d';

export interface IAppContext {
  subscribeForCurrentTrack(cb: (currentTrack: ITrack) => any): () => void;
  updateCurrentTrack(currentTrack: ITrack): void;
}

const subscriptions = new Set<(currentTrack: ITrack) => any>();

//export const AppContext = createContext<IAppContext>({
export const AppContext: IAppContext = {
  subscribeForCurrentTrack: cb => {subscriptions.add(cb); return () => subscriptions.delete(cb)},
  updateCurrentTrack: currentTrack => subscriptions.forEach(cb => cb(currentTrack)),
}
//});
