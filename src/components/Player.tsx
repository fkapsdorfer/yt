import * as React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import {useSnackbar} from 'notistack';
import {AppContext} from '../AppContext';

type PlayerProps = {focus: (fn: () => void) => void};

export default function Player(props: PlayerProps) {
  const audioEl = React.useRef<HTMLAudioElement>(null);
  //const {currentTrack} = React.useContext(AppContext);
  const [url, setUrl] = React.useState<string>(undefined);
  const {enqueueSnackbar} = useSnackbar();

  React.useEffect(() => props.focus(() => audioEl.current.focus()));
  React.useEffect(() => AppContext.subscribeForCurrentTrack(t => t.getUrl()
    .then(setUrl)
    .catch(e => {console.error(e.stack); enqueueSnackbar(e.massage)})));

  return (
    <Paper style={{position: 'fixed', bottom: 0, zIndex: 50, width: '100%'}}>
      <Grid container alignItems="center" style={{height: '4rem'}}>
        <style dangerouslySetInnerHTML={{
          __html: `
            audio::-webkit-media-controls-panel {
              background: #a66;
            }
          `
        }}></style>
        <audio
          controls
          src={url}
          onLoadedData={e => {
            audioEl.current.focus();
            e.currentTarget.play()
          }}
          style={{flexGrow: 1, width: '100%'}}
          ref={audioEl}
        ></audio>
      </Grid>
    </Paper>
  );
}
