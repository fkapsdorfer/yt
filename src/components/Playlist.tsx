import {debounce} from 'throttle-debounce';
import * as React from 'react';
import {RouteComponentProps} from 'react-router';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ArrowBack from '@material-ui/icons/ArrowBack';
import PlayArrow from '@material-ui/icons/PlayArrow';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Add from '@material-ui/icons/Add';
import SaveIcon from '@material-ui/icons/Save';
import {useSnackbar} from 'notistack';
import {AppContext} from '../AppContext';
import {IPlaylist, IPlaylistCollection} from '../store/playlist.d';
import {ITrack, ITrackCollection} from '../store/track.d';
import {TYPES} from '../types';
import {iocc} from '../iocc';

type PlaylistProps = RouteComponentProps<{playlist: string}>;

export default function Playlist(props: PlaylistProps) {
  const playlists: IPlaylistCollection = iocc.get<IPlaylistCollection>(TYPES.PlaylistCollection);
  const playlist: IPlaylist = playlists.get(props.match.params.playlist);
  const tracks: ITrackCollection = iocc.get<ITrackCollection>(TYPES.TrackCollection);

  const {enqueueSnackbar} = useSnackbar();
  const [newId, setNewId] = React.useState<string>('');
  const [filter, setFilter] = React.useState<string>('');
  const [records, setRecords] = React.useState<ITrack[]>(tracks.get(playlist.id));
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [trackUnderMenu, setTrackUnderMenu] = React.useState<ITrack>(null);

  const updateSelection = (f: string) => {
    const filterRe = new RegExp(f, 'i');
    const records = tracks.get(playlist.id)
      .filter(t => filterRe.test(t.title) || filterRe.test(t.uploader));
    f !== filter && setFilter(f);
    setRecords(records);
  }
  const onFilterChange = debounce(300, updateSelection);

  const onSelect = (id: string) => {
    const track = records.find(r => r.id === id);
    const trackClone = track.clone();
    trackClone.playlist = 'history';
    tracks.add(trackClone);
    AppContext.updateCurrentTrack(trackClone);
    if (props.match.params.playlist === 'history')
      setRecords(tracks.get(props.match.params.playlist));
  }

  const addTrack = () => {
    const extracted = newId.match(/https?:\/\/.*?[?&]v=([a-zA-Z0-9\-_]+)/);
    tracks
      .construct(playlist.id, extracted ? extracted[1] : newId)
      .then(() => {setNewId(''); updateSelection(filter)})
      .catch(e => {console.error(e.stack); enqueueSnackbar(e.message)});
  }

  const onRemoveTrack = () => {
    tracks.remove(trackUnderMenu);
    updateSelection(filter);
    setAnchorEl(null);
    setTrackUnderMenu(null);
  }

  const onOpenMenu = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, track: ITrack) => {
    setAnchorEl(e.currentTarget);
    setTrackUnderMenu(track);
  }

  const onCloseMenu = () => {
    setAnchorEl(null);
    setTrackUnderMenu(null);
  }

  return (
    <Table style={{marginBottom: '4rem'}}>
      <TableHead>
        <TableRow>
          <TableCell style={{padding: 0}}>
            <IconButton onClick={() => props.history.goBack()}>
              <ArrowBack fontSize="large" />
            </IconButton>
          </TableCell>
          <TableCell>Title</TableCell>
          <TableCell align="right">ID</TableCell>
          <TableCell align="right">Duration</TableCell>
          <TableCell align="right">Uploader</TableCell>
          <TableCell />
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow key="New">
          <TableCell align="left" style={{padding: 0}}>
            <IconButton
              onClick={() => addTrack()}
            >
              <Add fontSize="large" />
            </IconButton>
          </TableCell>
          <TableCell>
            <TextField
              label="Search"
              onChange={e => onFilterChange(e.target.value)}
            />
          </TableCell>
          <TableCell scope="row">
            <TextField
              label="Add New (ID)"
              onChange={e => setNewId(e.target.value)}
              value={newId}
            />
          </TableCell>
          <TableCell />
          <TableCell />
          <TableCell>
            <IconButton disabled={!tracks.canFlush(playlist.id)}>
              <SaveIcon />
            </IconButton>
          </TableCell>
        </TableRow>
        {records.map(row => (
          <TableRow key={row.id}>
            <TableCell align="left" style={{padding: 0}}>
              <IconButton onClick={() => onSelect(row.id)}>
                <PlayArrow fontSize="large" />
              </IconButton>
            </TableCell>
            <TableCell>{row.title}</TableCell>
            <TableCell align="right">{row.id}</TableCell>
            <TableCell align="right">{row.duration}</TableCell>
            <TableCell align="right">{row.uploader}</TableCell>
            <TableCell align="right">
              <IconButton onClick={e => onOpenMenu(e, row)}>
                <MoreVertIcon />
              </IconButton>
            </TableCell>
          </TableRow>
        ))}
      </TableBody>

      <Menu
        anchorEl={anchorEl}
        open={!!anchorEl}
        onClose={onCloseMenu}
      >
        <MenuItem onClick={onRemoveTrack}>Remove</MenuItem>
      </Menu>
    </Table>
  );
}
