import * as React from 'react';
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import Paper from '@material-ui/core/Paper';
import {SnackbarProvider} from 'notistack';
import {emitExit} from '../hooks';
import Navigation from './Navigation';
import Playlists from './Playlists';
import Playlist from './Playlist';
import Settings from './Settings';
import Player from './Player';

const theme = createMuiTheme({
  palette: {
    type: 'dark',
  }
});

export default class App extends React.Component<{}, {}> {

  focusPlayer: () => void = null;

  constructor(props: {}) {
    super(props);
    window.onbeforeunload = e => void emitExit();
    document.addEventListener('keyup', evt => {
      if (evt.key === 'Escape')
        this.focusPlayer();
    });
  }

  render() {
    return (
      <React.Fragment>
        <CssBaseline></CssBaseline>
        <MuiThemeProvider theme={theme}>
          <SnackbarProvider maxSnack={3}>
            <Paper style={{minHeight: '100%'}}>
              <Router>
                <Navigation />
                <Redirect from="/" to="/playlists" />
                <Route path="/playlists" component={Playlists} />
                <Route path="/playlist/:playlist" component={Playlist} />
                <Route path="/settings" component={Settings} />
              </Router>
              <Player focus={(fn: () => void) => this.focusPlayer = fn} />
            </Paper>
          </SnackbarProvider>
        </MuiThemeProvider>
      </React.Fragment>
    );
  }
}
