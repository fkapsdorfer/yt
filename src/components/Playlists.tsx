import * as React from 'react';
import {RouteComponentProps} from 'react-router';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Folder from '@material-ui/icons/Folder';
import Delete from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';
import {IPlaylistCollection} from '../store/playlist.d';
import {TYPES} from '../types';
import {iocc} from '../iocc';

type PlaylistsProps =  {playlist: string} & RouteComponentProps;
type PlaylistsState = {playlists: string[], newPlaylistName: string};

export default class Playlists extends React.Component<PlaylistsProps, PlaylistsState> {

  state = {playlists: [], newPlaylistName: ''};

  private _playlists: IPlaylistCollection = iocc.get<IPlaylistCollection>(TYPES.PlaylistCollection);

  constructor(props: PlaylistsProps) {
    super(props);
    this._load();
  }

  async _load() {
    this.setState({playlists: await this._playlists.get().map(p => p.id)});
  }

  render() {
    return (
      <Paper>
        <List>
          {this.state.playlists.map(playlistName => (
            <ListItem key={playlistName} button onClick={() => this.props.history.push(`/playlist/${playlistName}`)} >
              <ListItemAvatar>
                <Avatar>
                  <Folder />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={playlistName} />
              <ListItemSecondaryAction>
                <IconButton
                  edge="end"
                  onClick={async () => this.setState({
                    playlists: (await this._playlists.remove(playlistName)).get().map(p => p.id)
                  })}
                >
                  <Delete />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>

        <Paper>
          <TextField  
            variant="filled"
            label="Create Playlist"
            style={{width: '100%'}}
            value={this.state.newPlaylistName}
            onChange={e => this.setState({newPlaylistName: e.target.value})}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    edge="end"
                    onClick={() => this.setState({}, async () => this.setState({
                      playlists: (await this._playlists.add(this.state.newPlaylistName)).get().map(p => p.id),
                      newPlaylistName: '',
                    }))}
                  >
                    <Add />
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </Paper>
      </Paper>
    );
  }
}
