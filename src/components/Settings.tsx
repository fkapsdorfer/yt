import * as React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl/FormControl';
import TextField from '@material-ui/core/TextField';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import RestoreIcon from '@material-ui/icons/Restore';
import {ISettings} from '../store/settings.d';
import {ITrackCollection} from '../store/track.d';
import {ITrackUrlCollection} from '../store/track-url.d';
import {TYPES} from '../types';
import {iocc} from '../iocc';

const useStylesForSettings = makeStyles((theme: Theme) => createStyles({
  root: {
    padding: theme.spacing(1),
  },
  formControl: {
    marginTop: theme.spacing(1.5),
  },
  button: {
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(1),
  },
}));

const useSettingsState = (settings: ISettings) => {
  const {useState} = React;
  return {
    youtubeDl: useState(settings.youtubeDl),
    storageDir: useState(settings.storageDir),
    historyMaxLength: useState(settings.historyMaxLength),
    addingDirection: useState(settings.addingDirection),
    ttl: useState(settings.ttl),
  };
}

export default function Settings() {
  const settings = iocc.get<ISettings>(TYPES.Settings);
  const classes = useStylesForSettings();
  const state = useSettingsState(settings);
  const stateToParams = ([value, set]: [any, Function]) => ({value, onChange: (e: React.ChangeEvent<HTMLInputElement>) => set(e.target.value)});
  const reset = () => Object.entries(state).forEach(([k, [, set]]) => set(settings[k]));

  return (
    <Paper className={classes.root}>
      <SettingsFlushableButtons />

      <FormControl className={classes.formControl} fullWidth>
        <TextField label="youtube-dl" {...stateToParams(state.youtubeDl)} />
      </FormControl>
      <FormControl className={classes.formControl} fullWidth>
        <TextField label="Storage Directory" {...stateToParams(state.storageDir)} />
      </FormControl>
      <FormControl className={classes.formControl} fullWidth>
        <TextField label="History To Keep" {...stateToParams(state.historyMaxLength)} />
      </FormControl>
      <FormControl className={classes.formControl} fullWidth>
        <TextField label="Adding Direction" {...stateToParams(state.addingDirection)} />
      </FormControl>
      <FormControl className={classes.formControl} fullWidth>
        <TextField label="Time to Live (ms)" {...stateToParams(state.ttl)} />
      </FormControl>

      <Button
        onClick={reset}
        className={classes.button}
        variant="contained"
        color="secondary"
        startIcon={<RestoreIcon />}
      >
        Reset
      </Button>
      <Button
        className={classes.button}
        // TODO: add save action
        variant="contained"
        color="primary"
        startIcon={<SaveIcon />}
      >
        Save
      </Button>

    </Paper>
  );
}

const useStylesForFlushableButtons = makeStyles((theme: Theme) => createStyles({
  buttonGroup: {marginRight: theme.spacing(1)}
}));

function SettingsFlushableButtons() {
  const tracks = iocc.get<ITrackCollection>(TYPES.TrackCollection);
  const trackUrls = iocc.get<ITrackUrlCollection>(TYPES.TrackUrlCollection);

  const classes = useStylesForFlushableButtons();
  const [canFlushTracks, setTracks] = React.useState<boolean>(tracks.canFlush());
  const [canFlushUrls, setUrls] = React.useState<boolean>(trackUrls.canFlush());

  const saveTracks = () => tracks.save();
  const reloadTracks = () => tracks.load().then(() => setTracks(tracks.canFlush()));
  const saveUrls = () => trackUrls.save();
  const reloadUrls = () => trackUrls.load().then(() => setUrls(trackUrls.canFlush()));

  return (
    <React.Fragment>
      <ButtonGroup className={classes.buttonGroup}>
        <Button startIcon={<SaveIcon />} onClick={saveTracks} disabled={!canFlushTracks}>Flush Tracks</Button>
        <Button startIcon={<RestoreIcon />} onClick={reloadTracks}>Reload Tracks</Button>
      </ButtonGroup>
      <ButtonGroup className={classes.buttonGroup}>
        <Button startIcon={<SaveIcon />} onClick={saveUrls} disabled={!canFlushUrls}>Flush Track URLs</Button>
        <Button startIcon={<RestoreIcon />} onClick={reloadUrls}>Reload Track URLs</Button>
      </ButtonGroup>
    </React.Fragment>
  );
}
