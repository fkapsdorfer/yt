import * as React from 'react';
import {useHistory, useLocation} from "react-router-dom";
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {ITrack} from '../store/track.d';
import {AppContext} from '../AppContext';

const useStyles = makeStyles((theme: Theme) => createStyles({
  tabCurrentTrack: {
    maxWidth: 'none',
  },
}));

const mapping = {
  '/playlists': 'Playlists',
  '/settings': 'Settings',
};
const mapLocation = (p: string) => mapping[p] ? p : false;

export default function Navigation() {
  const classes = useStyles();
  const history = useHistory();
  const location = useLocation();

  const [track, setTrack] = React.useState<ITrack>(null);
  React.useEffect(() => AppContext.subscribeForCurrentTrack(track => {
    window.document.title = `YT Player - ${track.title}`;
    setTrack(track);
  }));

  const toTabParams = ([path, label]: [string, string]) => ({
    label, value: path, key: path, onClick: () => history.push(path),
  });

  return (
    <AppBar position="static" color="default">
      <Tabs
        value={mapLocation(location.pathname)}
        variant="scrollable"
        scrollButtons="on"
        indicatorColor="primary"
        textColor="primary"
      >
        {Object.entries(mapping).map(entry => (
          <Tab {...toTabParams(entry)} />)
        )}
        {track && (<Tab disabled={true} className={classes.tabCurrentTrack} label={`⯈ ${track.title}`} />)}
      </Tabs>
    </AppBar>
  );
}
