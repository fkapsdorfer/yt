import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'reflect-metadata';
import './inversify.config';
import {initPersistence} from './store/initialize';
import App from './components/App';

(async () => {
  await initPersistence();
  ReactDOM.render(
    <App />,
    document.querySelector('#root'),
  );
})();
