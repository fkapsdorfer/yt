import {app, BrowserWindow} from 'electron';

let mainWindow: BrowserWindow;

main();

function main() {

  app.on('ready', () => {
    createWindow(); 
  });

  app.on('activate', () => mainWindow === null && createWindow());
  app.on('window-all-closed', () => process.platform !== 'darwin' && app.quit());
}

async function createWindow() {
  mainWindow = new BrowserWindow({
    icon: `${__dirname}/../icons/favicon-32x32.png`,
    webPreferences: {
      nodeIntegration: true,
    },
  });

  mainWindow.on('closed', () => mainWindow = null);
  //mainWindow.loadURL(`file://./index.html`);
  mainWindow.loadFile('./index.html');
}
