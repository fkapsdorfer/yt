// See https://github.com/ytdl-org/youtube-dl/blob/master/README.md#output-template.

export class FormatAsDownloaded {
  /** A short description of the format */
  format_id: string;
  
  /** A human-readable description of the format */
  format: string;
 
  /** Additional info about the format */
  format_note: string;

  /** */
  quality: number;
  
  /** The protocol that will be used for the actual download,
   * lower-case (http, https, rtsp, rtmp, rtmpe, mms, f4m, ism,
   * http_dash_segments, m3u8, or m3u8_native) */
  protocol: string;

  /** Video URL */
  url: string;
  
  /** */
  player_url: string;

  /** */
  http_headers: {[s: string]: string};
  

  /** Video filename extension */
  ext: string;
  
  /** The number of bytes, if known in advance */
  filesize: number;
  

  /** Name of the audio codec in use */
  acodec: string;
  
  /** Average audio bitrate in KBit/s */
  abr: number;
  
  /** Audio sampling rate in Hertz */
  asr: number;
  
  
  /** Name of the video codec in use */
  vcodec: string;
    
  /** Width of the video */
  width: number;

  /** Height of the video */
  height: number;

  /** Frame rate */
  fps: number;

  /** Average bitrate of audio and video in KBit/s */
  tbr: number;
}

export class TrackAsDownloaded {
  id: string;
  uploader: string;
  uploader_id: string;
  uploader_url: string;
  channel_id: string;
  channel_url: string;
  /** YYYYMMDD */
  upload_date: string;
  /** Not yet known. (null) */
  license: any;
  creator: string;
  title: string;
  alt_title: string;
  thumbnail: string;
  description: string;
  categories: {[s: string]: string};
  tags: {[s: string]: string};
  /** Not yet known. ({}) */
  subtitles: any;
  /** Not yet known. ({}) */
  automatic_captions: any;
  /** Not yet known. (null) */
  annotations: null;
  duration: number;
  age_limit: number;
  /** Not yet known. (null) */
  chapters: any;
  webpage_url: string;
  view_count: number;
  like_count: number;
  dislike_count: number;
  /** Decimal from 0 (worst) to 5 (best) based on (dis)likes */
  average_rating: number;
  formats: FormatAsDownloaded[];
  format: string;
  format_id: string;
  width: string;
  height: string;
  /** Not yet known. (null) */
  resolution: any;
  fps: number;
  vcodec: string;
  /** Not yet known. (null) */
  vbr: any;
  /** Not yet known. (null) */
  stretched_ratio: any;
  acodec: string;
  abr: number;
  ext: string;
  fulltitle: string;
  _filename: string;
}
