
export const TYPES = {
  Settings: Symbol.for('Settings'),
  TrackUrl: Symbol.for('Newable<TrackUrl>'),
  TrackUrlCollection: Symbol.for('TrackUrlCollection'),
  Track: Symbol.for('Newable<Track>'),
  TrackCollection: Symbol.for('TrackCollection'),
  Playlist: Symbol.for('Newable<Playlist>'),
  PlaylistCollection: Symbol.for('PlaylistCollection'),
  YtService: Symbol.for('YtService'),
  Registry: Symbol.for('Registry'),
};
